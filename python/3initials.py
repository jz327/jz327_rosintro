# Python 2/3 compatibility imports
from __future__ import print_function

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

## END_SUB_TUTORIAL


def all_close(goal, actual, tolerance):
    """
    Convenience method for testing if the values in two lists are within a tolerance of each other.
    For Pose and PoseStamped inputs, the angle between the two quaternions is compared (the angle
    between the identical orientations q and -q is calculated correctly).
    @param: goal       A list of floats, a Pose or a PoseStamped
    @param: actual     A list of floats, a Pose or a PoseStamped
    @param: tolerance  A float
    @returns: bool
    """
    if type(goal) is list:
        for index in range(len(goal)):
            if abs(actual[index] - goal[index]) > tolerance:
                return False

    elif type(goal) is geometry_msgs.msg.PoseStamped:
        return all_close(goal.pose, actual.pose, tolerance)

    elif type(goal) is geometry_msgs.msg.Pose:
        x0, y0, z0, qx0, qy0, qz0, qw0 = pose_to_list(actual)
        x1, y1, z1, qx1, qy1, qz1, qw1 = pose_to_list(goal)
        # Euclidean distance
        d = dist((x1, y1, z1), (x0, y0, z0))
        # phi = angle between orientations
        cos_phi_half = fabs(qx0 * qx1 + qy0 * qy1 + qz0 * qz1 + qw0 * qw1)
        return d <= tolerance and cos_phi_half >= cos(tolerance / 2.0)

    return True


class MidtermGroupPythonInterface(object):
    """MidtermGroupPythonInterface"""

    def __init__(self):
        super(MidtermGroupPythonInterface, self).__init__()

        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("move_group_python_interface_tutorial", anonymous=True)
        robot = moveit_commander.RobotCommander()
        scene = moveit_commander.PlanningSceneInterface()
        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)

        display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )

        planning_frame = move_group.get_planning_frame()
        eef_link = move_group.get_end_effector_link()
        group_names = robot.get_group_names()

        # Misc variables
        self.box_name = ""
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.display_trajectory_publisher = display_trajectory_publisher
        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names


        self.letter_width = 0.25
        self.letter_height = 0.4

    # Goes to a state with joint values
    def go_to_joint_state(self, state):
        move_group = self.move_group

        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = state[0]
        joint_goal[1] = state[1]
        joint_goal[2] = state[2]
        joint_goal[3] = state[3]
        joint_goal[4] = state[4]
        joint_goal[5] = state[5]

        move_group.go(joint_goal, wait=True)

        move_group.stop()

        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)

    def plan_Z(self, scale=0.2):
        move_group = self.move_group

        waypoints = []

        wpose = move_group.get_current_pose().pose

        wpose.position.y += scale * -self.letter_width
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.y += scale * self.letter_width
        wpose.position.z += scale * -self.letter_height
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.y += scale * -self.letter_width
        waypoints.append(copy.deepcopy(wpose))

        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold

        return plan, fraction

    def plan_J(self, scale=0.2):
        move_group = self.move_group

        waypoints = []

        wpose = move_group.get_current_pose().pose

        # Top bar
        wpose.position.y += scale * -self.letter_width
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.y += scale * self.letter_width * 2
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.y += scale * -self.letter_width
        waypoints.append(copy.deepcopy(wpose))

        # Trunk of J
        wpose.position.z += scale * -self.letter_height
        waypoints.append(copy.deepcopy(wpose))

        # Curl of J
        wpose.position.y += scale * self.letter_width
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z += scale * self.letter_height / 2
        waypoints.append(copy.deepcopy(wpose))

        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold

        return plan, fraction
    

    # Plans cartesian coordinates to move over for the next letter
    def plan_move_over(self, move_y, scale=0.2):
        move_group = self.move_group

        waypoints = []

        wpose = move_group.get_current_pose().pose

        wpose.position.y += scale * -move_y
        wpose.position.z += scale * self.letter_height / 2
        waypoints.append(copy.deepcopy(wpose))

        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold

        return plan, fraction
    
    def plan_move_in_out(self, direction, scale=0.2):
        move_group = self.move_group

        waypoints = []

        wpose = move_group.get_current_pose().pose

        if direction == "in":
            wpose.position.x += scale * 0.2 
        else:
            wpose.position.x += scale * -0.2
        waypoints.append(copy.deepcopy(wpose))

        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold

        return plan, fraction


    def execute_plan(self, plan):
        move_group = self.move_group
        move_group.execute(plan, wait=True)


def main():
    try:
        robot = MidtermGroupPythonInterface()
        
        # tau = 2 pi
        robot.go_to_joint_state([0, -tau/6, tau/6, tau/2, -tau/4, 0])

        # print joint values, [point 1]
        print("[Point 1] Joint Values: ",robot.move_group.get_current_joint_values())

        # Letters will be in a box of 0.25 width and 0.4 height

         # # Move over
        cartesian_plan_move_over, fraction = robot.plan_move_over(-0.4)
        robot.execute_plan(cartesian_plan_move_over)

        # Move in and start writing
        cartesian_plan_move_in, fraction = robot.plan_move_in_out("in")
        robot.execute_plan(cartesian_plan_move_in)
        
        # Draw a J
        cartesian_plan_J, fraction = robot.plan_J()
        robot.execute_plan(cartesian_plan_J)

        # print joint values, [point 2]
        print("[Point 2] Joint Values: ", robot.move_group.get_current_joint_values())
   
        # Move out
        cartesian_plan_move_in, fraction = robot.plan_move_in_out("out")
        robot.execute_plan(cartesian_plan_move_in)

        # # Move over
        cartesian_plan_move_over, fraction = robot.plan_move_over(1)
        robot.execute_plan(cartesian_plan_move_over)

        # Move in
        cartesian_plan_move_in, fraction = robot.plan_move_in_out("in")
        robot.execute_plan(cartesian_plan_move_in)

        # Draw a J
        cartesian_plan_J, fraction = robot.plan_J()
        robot.execute_plan(cartesian_plan_J)

        # Move out
        cartesian_plan_move_in, fraction = robot.plan_move_in_out("out")
        robot.execute_plan(cartesian_plan_move_in)

        # # Move over
        cartesian_plan_move_over, fraction = robot.plan_move_over(1)
        robot.execute_plan(cartesian_plan_move_over)

        # Move in
        cartesian_plan_move_in, fraction = robot.plan_move_in_out("in")
        robot.execute_plan(cartesian_plan_move_in)

        # # Draw a Z
        cartesian_plan_Z, fraction = robot.plan_Z()
        robot.execute_plan(cartesian_plan_Z)

        # Move out
        cartesian_plan_move_in, fraction = robot.plan_move_in_out("out")
        robot.execute_plan(cartesian_plan_move_in)

        # print joint values, [point 3]
        print("[Point 3] Joint Values: ", robot.move_group.get_current_joint_values())


    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return


if __name__ == "__main__":
    main()